import 'package:flutter/material.dart';
import 'package:buscador_gif/screens/home_page.dart';

main() {
  runApp(MaterialApp(
    home: HomePage(),
    theme: ThemeData(hintColor: Colors.white, primaryColor: Colors.white),
  ));
}
