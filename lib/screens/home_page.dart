import 'dart:convert';

import 'package:buscador_gif/screens/show_gifs.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:share/share.dart';
import 'package:transparent_image/transparent_image.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  String _search;
  int _offset = 0;

  Future<Map> _getGifs() async {
    http.Response response;

    if (_search == null) {
      response = await http.get(
          'https://api.giphy.com/v1/gifs/trending?api_key=RQXEQnG2vd5C5I8QAdhMIkQ2JGc3xJES&limit=24&rating=G');
    } else {
      response = await http.get(
          'https://api.giphy.com/v1/gifs/search?api_key=RQXEQnG2vd5C5I8QAdhMIkQ2JGc3xJES&q=$_search&limit=23&offset=$_offset&rating=G&lang=pl');
    }

    return json.decode(response.body);
  }

  _getCount(List data) {
    if (_search == null)
      return data.length;
    else
      return data.length + 1;
  }

  @override
  void initState() {
    super.initState();

    _getGifs().then((map) {
      print(map);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: Image.network(
          'https://developers.giphy.com/static/img/dev-logo-lg.7404c00322a8.gif',
          height: 30.0,
        ),
        centerTitle: true,
      ),
      body: Column(
        children: <Widget>[
          Container(
            padding: EdgeInsets.all(20.0),
            child: TextField(
                onSubmitted: (text) {
                  setState(() {
                    _search = text;
                  });
                },
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: "Busque um gif",
                    labelStyle: TextStyle(color: Colors.white, fontSize: 20.0)),
                style: TextStyle(
                    fontWeight: FontWeight.bold, color: Colors.white)),
          ),
          Expanded(
            child: FutureBuilder(
              future: _getGifs(),
              builder: (context, snapshot) {
                switch (snapshot.connectionState) {
                  case ConnectionState.waiting:
                  case ConnectionState.none:
                    return Container(
                      width: 200.0,
                      height: 200.0,
                      alignment: Alignment.center,
                      child: CircularProgressIndicator(
                        valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                        strokeWidth: 5.0,
                      ),
                    );
                  default:
                    if (snapshot.hasError || snapshot.data['data'].length == 0)
                      return Container();
                    else
                      return _createTableGif(context, snapshot);
                }
              },
            ),
          )
        ],
      ),
    );
  }

  Widget _createTableGif(BuildContext context, AsyncSnapshot snapshot) {
    return GridView.builder(
        padding: EdgeInsets.all(20.0),
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 2,
          crossAxisSpacing: 10.0,
          mainAxisSpacing: 10.0,
        ),
        itemCount: _getCount(snapshot.data['data']),
        itemBuilder: (context, index) {
          if (_search == null || index < snapshot.data['data'].length)
            return GestureDetector(
              child: FadeInImage.memoryNetwork(
                  placeholder: kTransparentImage,
                  image: snapshot.data['data'][index]['images']['fixed_height']
                      ['url'],
                  height: 300.0,
                  fit: BoxFit.cover),
              onTap: () {
                Navigator.push(
                    context,
                    (MaterialPageRoute(
                        builder: (context) =>
                            ShowGifs(snapshot.data['data'][index]))));
              },
              onLongPress: () {
                Share.share(
                  snapshot.data['data'][index]['images']['fixed_height']['url'],
                );
              },
            );
          else
            return GestureDetector(
              onTap: () {
                setState(() {
                  _offset += 23;
                });
              },
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Icon(Icons.control_point, color: Colors.white, size: 60.0),
                  Text(
                    "Carregue mais",
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 10.0),
                  )
                ],
              ),
            );
        });
  }
}
